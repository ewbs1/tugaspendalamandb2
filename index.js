//use path module
const path = require('path');
//use express module
const express = require('express');
//use hbs view engine
const hbs = require('hbs');
//use bodyParser middleware
const bodyParser = require('body-parser');
//cors
const cors = require('cors')
//firebase
const firebaseapp = require('firebase/app');
const firebase = require('firebase');
const firestore = require('firebase/firestore');

const app = express();
app.use(cors())
app.use(bodyParser.json())

//<!-- The core Firebase JS SDK is always required and must be listed first -->
//<script src="https://www.gstatic.com/firebasejs/8.0.0/firebase-app.js"></script>

//<!-- TODO: Add SDKs for Firebase products that you want to use
  //   https://firebase.google.com/docs/web/setup#available-libraries -->
//<script src="https://www.gstatic.com/firebasejs/8.0.0/firebase-analytics.js"></script>

//<script>
  // Your web app's Firebase configuration
  // For Firebase JS SDK v7.20.0 and later, measurementId is optional
  var firebaseConfig = {
    apiKey: "AIzaSyBJyAGsc9ktH6FG7OoRSGfPlj-8f-bKQ1Y",
    authDomain: "tugaspendalamandb2.firebaseapp.com",
    databaseURL: "https://tugaspendalamandb2.firebaseio.com",
    projectId: "tugaspendalamandb2",
    storageBucket: "tugaspendalamandb2.appspot.com",
    messagingSenderId: "886086238451",
    appId: "1:886086238451:web:e88328bfaf38d8012ef786",
    measurementId: "G-4LJFXW0QKE"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);

 // firebase.analytics();
//</script>

// db and firestore
var db = firebase.database()
var fs = firebase.firestore()

//set views file
app.set('views',path.join(__dirname,'views'));
//set view engine
app.set('view engine', 'hbs');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
//set folder public as static folder for static file
app.use('/assets',express.static(__dirname + '/public'));

//route untuk homepage
app.get('/',(req, res) => {
    res.send('<pre>halaman antrian ada di url /antrian/{service}</pre>')    
});

let csRef = fs.collection('antrian').doc('customerservice');
let telRef = fs.collection('antrian').doc('teller');

app.get('/antrian/customerservice',(req, res) => {
    fs.settings({
        timestampsInSnapshots:true
    })
    var allData = []
    csRef.onSnapshot(docSnapshot => {
        //console.log(`Received doc snapshot: ${docSnapshot.data()}`);
        var dat = docSnapshot.data()
        res.send(dat)
      }, err => {
        console.log(`Encountered error: ${err}`);
      });
    })

    app.get('/antrian/teller',(req, res) => {
        fs.settings({
            timestampsInSnapshots:true,
        })
        telRef.onSnapshot(docSnapshot => {
                //console.log(`Received doc snapshot: ${docSnapshot.data()}`);
            var dat = docSnapshot.data()
            res.send(dat)
        }, err => {
            console.log(`Encountered error: ${err}`);
        });
    })
    
    app.listen(1700, () => {
        console.log('Server is running at port 1700');
    });
